3.1.16 (Brainy Betty)
b2f00c7555b91fdd95b4f3fc272daa4447c41a83
o:Sass::Tree::RootNode
:@template"q/**
 * @class Ext.dataview.List
 */

.x-list {
    background-color: $list-bg-color;

    .x-list-disclosure {
        position: relative;
        overflow: visible;
        border: 0;
        @include border-radius($list-disclosure-size);
        @include background-gradient(saturate($active-color, 10%));
        width: $list-disclosure-size;
        height: $list-disclosure-size;
        margin: 7px 7px 0 0;

        &:before {
            @include absolute-position(0, 0, 0, 0);
            content: ']';
            font-family: 'Pictos';
            color: #fff;
            font-size: 24px;
            text-align: center;
            line-height: 35px;
            text-shadow: 0 0 0;
        }
    }

    &.x-list-indexed .x-list-disclosure {
        margin-right: 1.8em;
    }

    .x-item-selected .x-list-disclosure {
        background: #fff none;

        &:before {
            color: $active-color;
        }
    }

    .x-list-item {
        color: $list-color;

        &.x-item-pressed {
            &.x-list-item-tpl,
            .x-dock-horizontal {
                background: $list-pressed-color none;
            }
        }

        &.x-item-selected .x-dock-horizontal,
        &.x-item-selected.x-list-item-tpl {
            @include background-gradient($list-active-color, $list-active-gradient);
            @include color-by-background($list-active-color);
        }

        .x-list-item-body,
        &.x-list-item-tpl .x-innerhtml {
            padding: 12px 15px;
        }
    }
}

.x-list-normal {
    .x-list-header {
        @include background-gradient($list-header-bg-color, $list-header-gradient);
        @include color-by-background($list-header-bg-color, 30%);
        @include bevel-by-background($list-header-bg-color);
        border-top: 1px solid $list-header-bg-color;
        border-bottom: 1px solid darken($list-header-bg-color, 20%);
        font-weight: bold;
        font-size: 0.8em;
        padding: 0.2em 1.02em;
    }

    .x-list-item {
        &.x-list-item-tpl,
        .x-dock-horizontal {
            border-top: 1px solid darken($list-bg-color, 10%);
        }


        &.x-list-item-tpl.x-list-footer-wrap,
        &.x-list-footer-wrap .x-dock-horizontal {
            border-bottom: 1px solid darken($list-bg-color, 10%);
        }
    }

    .x-list-item {
        @if $include-list-highlights {
            &.x-item-pressed.x-list-item-tpl,
            &.x-item-pressed .x-dock-horizontal {
                border-top-color: $list-pressed-color;
                background-color: $list-pressed-color;
            }


            &.x-item-selected.x-list-item-tpl,
            &.x-item-selected .x-dock-horizontal {
                border-top-color: $list-active-color;
            }
        }
    }
}

.x-list-round {
    .x-scroll-view {
        background-color: #eee;
    }

    .x-list-header-swap {
        padding-right: $list-round-padding;
    }

    .x-list-inner .x-scroll-container {
        top: $list-round-padding;
        left: $list-round-padding;
        bottom: $list-round-padding;
        right: $list-round-padding;
        width: auto !important;
        height: auto !important;
    }

    .x-list-header {
        color: #777;
        font-size: 1em;
        font-weight: bold;
        padding-left: 26px;
        line-height: 1.7em;
        @include background-image(linear-gradient(top, rgba(238, 238, 238, 1), rgba(238, 238, 238, .9) 30%, rgba(238, 238, 238, .4)));
    }

    .x-list-container {
        padding: $list-round-padding $list-round-padding 0 $list-round-padding;

        .x-list-header {
            padding-left: $list-round-padding;
            background-image: none;
        }
    }

    &.x-list-ungrouped,
    &.x-list-grouped {
        .x-list-item-tpl,
        .x-list-item .x-dock-horizontal {
            border: 1px solid darken($list-bg-color, 10%);
            border-width: 1px 1px 0 1px;
            background: $list-bg-color;
        }
    }

    &.x-list-ungrouped {
        .x-list-item-first  {
            @if $include-border-radius {
                @include border-top-radius($list-rounded-radius);
            }
        }

        .x-list-item-last  {
            @if $include-border-radius {
                @include border-bottom-radius($list-rounded-radius);
            }
            border-width: 1px;
            margin-bottom: $list-round-padding;
        }
    }

    &.x-list-grouped {
        .x-list-header-wrap {
            .x-dock-horizontal {
                @if $include-border-radius {
                    @include border-top-radius($list-rounded-radius);
                }
            }
        }

        .x-list-header-wrap.x-list-header {
            border: 1px solid darken($list-bg-color, 10%);
            border-width: 1px 1px 0 1px;
            @if $include-border-radius {
                @include border-top-radius($list-rounded-radius);
            }
        }

        .x-list-footer-wrap {
            background: transparent;

            &.x-list-item-tpl,
            .x-dock-horizontal {
                border: none;
                background: transparent;
                padding-bottom: $list-round-padding;
                margin-bottom: $list-round-padding;

                > .x-innerhtml,
                > .x-dock-body {
                    border: 1px solid darken($list-bg-color, 10%);
                    background: $list-bg-color;
                    @if $include-border-radius {
                        @include border-bottom-radius($list-rounded-radius);
                    }
                }
            }

            &.x-item-selected {
                > .x-innerhtml,
                > .x-dock-body {
                    @include background-gradient($list-active-color, $list-active-gradient);
                    @include color-by-background($list-active-color);
                }
            }
        }
    }

    .x-indexbar-vertical {
        margin-right: 20px;
    }
}


.x-list-round .x-list-footer-wrap.x-list-item-last.x-list-item-odd.x-list-item.x-list-item-tpl {
    background-color: transparent !important;
}

.x-list-round.x-list-grouped .x-list-item-odd.x-list-footer-wrap {
    > .x-innerhtml,
    > .x-dock-body {
        background-color: darken($list-bg-color, 5%) !important;
    }
}

.x-list .x-list-item-odd {
    &.x-list-item-tpl,
    .x-dock-horizontal {
        background-color: darken($list-bg-color, 5%) !important;
        border-bottom: 1px solid darken($list-bg-color, 5%);
    }
}
:@has_childrenT:@options{ :@children[o:Sass::Tree::CommentNode;@:
@loud0;	[ :@value["(/**
 * @class Ext.dataview.List
 */:
@linei:@silent0o:Sass::Tree::RuleNode;T:
@rule[".x-list;@:@parsed_ruleso:"Sass::Selector::CommaSequence:@filename" ;i
:@members[o:Sass::Selector::Sequence;[o:#Sass::Selector::SimpleSequence;@;i
;[o:Sass::Selector::Class:
@name["x-list;@;i
;	[
o:Sass::Tree::PropNode;["background-color:@prop_syntax:new;@;	[ ;o:Sass::Script::Variable	:@underscored_name"list_bg_color;"list-bg-color;@;i;i:
@tabsi o;;T;[".x-list-disclosure;@;o;;" ;i;[o;;[o;;@&;i;[o;;["x-list-disclosure;@&;i;	[o;;["position;;;@;	[ ;o:Sass::Script::String:
@type:identifier;@;"relative;i;i o;;["overflow;;;@;	[ ;o;; ;!;@;"visible;i;i o;;["border;;;@;	[ ;o;; ;!;@;"0;i;i o:Sass::Tree::MixinNode;"border-radius:@keywords{ ;@;	[ ;i:
@args[o;	;"list_disclosure_size;"list-disclosure-size;@;io;";"background-gradient;#{ ;@;	[ ;i;$[o:Sass::Script::Funcall
;"saturate;#{ ;@;i;$[o;	;"active_color;"active-color;@;io:Sass::Script::Number:@numerator_units["%;@:@original"10%:@denominator_units[ ;i;io;;["
width;;;@;	[ ;o;	;"list_disclosure_size;"list-disclosure-size;@;i;i;i o;;["height;;;@;	[ ;o;	;"list_disclosure_size;"list-disclosure-size;@;i;i;i o;;["margin;;;@;	[ ;o;; ;!;@;"7px 7px 0 0;i;i o;;T;["&:before;@;o;;" ;i;[o;;[o;;@s;i;[o:Sass::Selector::Parent;@s;io:Sass::Selector::Pseudo
:	@arg0;["before; :
class;@s;i;	[o;";"absolute-position;#{ ;@;	[ ;i;$[	o;&;'[ ;@;("0;)[ ;i ;io;&;'[ ;@;("0;)@�;i ;io;&;'[ ;@;("0;)@�;i ;io;&;'[ ;@;("0;)@�;i ;io;;["content;;;@;	[ ;o;; ;!;@;"']';i;i o;;["font-family;;;@;	[ ;o;; ;!;@;"'Pictos';i;i o;;["
color;;;@;	[ ;o;; ;!;@;"	#fff;i;i o;;["font-size;;;@;	[ ;o;; ;!;@;"	24px;i;i o;;["text-align;;;@;	[ ;o;; ;!;@;"center;i;i o;;["line-height;;;@;	[ ;o;; ;!;@;"	35px;i;i o;;["text-shadow;;;@;	[ ;o;; ;!;@;"
0 0 0;i;i ;i;i ;i;i o;;T;["(&.x-list-indexed .x-list-disclosure;@;o;;" ;i#;[o;;[o;;@�;i#;[o;*;@�;i#o;;["x-list-indexed;@�;i#o;;@�;i#;[o;;["x-list-disclosure;@�;i#;	[o;;["margin-right;;;@;	[ ;o;; ;!;@;"
1.8em;i$;i ;i#;i o;;T;["(.x-item-selected .x-list-disclosure;@;o;;" ;i';[o;;[o;;@�;i';[o;;["x-item-selected;@�;i'o;;@�;i';[o;;["x-list-disclosure;@�;i';	[o;;["background;;;@;	[ ;o;; ;!;@;"#fff none;i(;i o;;T;["&:before;@;o;;" ;i*;[o;;[o;;@�;i*;[o;*;@�;i*o;+
;,0;["before; ;-;@�;i*;	[o;;["
color;;;@;	[ ;o;	;"active_color;"active-color;@;i+;i+;i ;i*;i ;i';i o;;T;[".x-list-item;@;o;;" ;i/;[o;;[o;;@;i/;[o;;["x-list-item;@;i/;	[	o;;["
color;;;@;	[ ;o;	;"list_color;"list-color;@;i0;i0;i o;;T;["&.x-item-pressed;@;o;;" ;i2;[o;;[o;;@;i2;[o;*;@;i2o;;["x-item-pressed;@;i2;	[o;;T;["6&.x-list-item-tpl,
            .x-dock-horizontal;@;o;;" ;i4;[o;;[o;;@&;i4;[o;*;@&;i4o;;["x-list-item-tpl;@&;i4o;;["
o;;@&;i4;[o;;["x-dock-horizontal;@&;i4;	[o;;["background;;;@;	[ ;o:Sass::Script::List	:@separator:
space;@;[o;	;"list_pressed_color;"list-pressed-color;@;i5o;	; ;!;@;"	none;i5;i5;i5;i ;i4;i ;i2;i o;;T;["T&.x-item-selected .x-dock-horizontal,
        &.x-item-selected.x-list-item-tpl;@;o;;" ;i:;[o;;[o;;@H;i:;[o;*;@H;i:o;;["x-item-selected;@H;i:o;;@H;i:;[o;;["x-dock-horizontal;@H;i:o;;["
o;;@H;i:;[o;*;@H;i:o;;["x-item-selected;@H;i:o;;["x-list-item-tpl;@H;i:;	[o;";"background-gradient;#{ ;@;	[ ;i;;$[o;	;"list_active_color;"list-active-color;@;i;o;	;"list_active_gradient;"list-active-gradient;@;i;o;";"color-by-background;#{ ;@;	[ ;i<;$[o;	;"list_active_color;"list-active-color;@;i<;i:;i o;;T;[">.x-list-item-body,
        &.x-list-item-tpl .x-innerhtml;@;o;;" ;i@;[o;;[o;;@{;i@;[o;;["x-list-item-body;@{;i@o;;["
o;;@{;i@;[o;*;@{;i@o;;["x-list-item-tpl;@{;i@o;;@{;i@;[o;;["x-innerhtml;@{;i@;	[o;;["padding;;;@;	[ ;o;; ;!;@;"12px 15px;iA;i ;i@;i ;i/;i ;i
;i o;;T;[".x-list-normal;@;o;;" ;iF;[o;;[o;;@�;iF;[o;;["x-list-normal;@�;iF;	[o;;T;[".x-list-header;@;o;;" ;iG;[o;;[o;;@�;iG;[o;;["x-list-header;@�;iG;	[o;";"background-gradient;#{ ;@;	[ ;iH;$[o;	;"list_header_bg_color;"list-header-bg-color;@;iHo;	;"list_header_gradient;"list-header-gradient;@;iHo;";"color-by-background;#{ ;@;	[ ;iI;$[o;	;"list_header_bg_color;"list-header-bg-color;@;iIo;&;'["%;@;("30%;)[ ;i#;iIo;";"bevel-by-background;#{ ;@;	[ ;iJ;$[o;	;"list_header_bg_color;"list-header-bg-color;@;iJo;;["border-top;;;@;	[ ;o;.	;/;0;@;[o;&;'["px;@;("1px;)[ ;i;iKo;	; ;!;@;"
solid;iKo;	;"list_header_bg_color;"list-header-bg-color;@;iK;iK;iK;i o;;["border-bottom;;;@;	[ ;o;.	;/;0;@;[o;&;'["px;@;("1px;)[ ;i;iLo;	; ;!;@;"
solid;iLo;%
;"darken;#{ ;@;iL;$[o;	;"list_header_bg_color;"list-header-bg-color;@;iLo;&;'["%;@;("20%;)[ ;i;iL;iL;iL;i o;;["font-weight;;;@;	[ ;o;; ;!;@;"	bold;iM;i o;;["font-size;;;@;	[ ;o;; ;!;@;"
0.8em;iN;i o;;["padding;;;@;	[ ;o;; ;!;@;"0.2em 1.02em;iO;i ;iG;i o;;T;[".x-list-item;@;o;;" ;iR;[o;;[o;;@;iR;[o;;["x-list-item;@;iR;	[o;;T;["2&.x-list-item-tpl,
        .x-dock-horizontal;@;o;;" ;iT;[o;;[o;;@";iT;[o;*;@";iTo;;["x-list-item-tpl;@";iTo;;["
o;;@";iT;[o;;["x-dock-horizontal;@";iT;	[o;;["border-top;;;@;	[ ;o;.	;/;0;@;[o;&;'["px;@;("1px;)[ ;i;iUo;	; ;!;@;"
solid;iUo;%
;"darken;#{ ;@;iU;$[o;	;"list_bg_color;"list-bg-color;@;iUo;&;'["%;@;("10%;)[ ;i;iU;iU;iU;i ;iT;i o;;T;["Z&.x-list-item-tpl.x-list-footer-wrap,
        &.x-list-footer-wrap .x-dock-horizontal;@;o;;" ;iZ;[o;;[o;;@R;iZ;[o;*;@R;iZo;;["x-list-item-tpl;@R;iZo;;["x-list-footer-wrap;@R;iZo;;["
o;;@R;iZ;[o;*;@R;iZo;;["x-list-footer-wrap;@R;iZo;;@R;iZ;[o;;["x-dock-horizontal;@R;iZ;	[o;;["border-bottom;;;@;	[ ;o;.	;/;0;@;[o;&;'["px;@;("1px;)[ ;i;i[o;	; ;!;@;"
solid;i[o;%
;"darken;#{ ;@;i[;$[o;	;"list_bg_color;"list-bg-color;@;i[o;&;'["%;@;("10%;)[ ;i;i[;i[;i[;i ;iZ;i ;iR;i o;;T;[".x-list-item;@;o;;" ;i_;[o;;[o;;@�;i_;[o;;["x-list-item;@�;i_;	[u:Sass::Tree::IfNodes[o:Sass::Script::Variable	:@underscored_name"include_list_highlights:
@name"include-list-highlights:@options{ :
@linei`0[o:Sass::Tree::RuleNode:@has_childrenT:
@rule["V&.x-item-pressed.x-list-item-tpl,
            &.x-item-pressed .x-dock-horizontal;@	:@parsed_ruleso:"Sass::Selector::CommaSequence:@filename" ;	ib:@members[o:Sass::Selector::Sequence;[o:#Sass::Selector::SimpleSequence;@;	ib;[o:Sass::Selector::Parent;@;	ibo:Sass::Selector::Class;["x-item-pressed;@;	ibo;;["x-list-item-tpl;@;	ibo;;["
o;;@;	ib;[o;;@;	ibo;;["x-item-pressed;@;	ibo;;@;	ib;[o;;["x-dock-horizontal;@;	ib:@children[o:Sass::Tree::PropNode;["border-top-color:@prop_syntax:new;@	;[ :@valueo; 	;"list_pressed_color;"list-pressed-color;@	;	ic;	ic:
@tabsi o;;["background-color;;;@	;[ ;o; 	;"list_pressed_color;"list-pressed-color;@	;	id;	id;i ;	ib;i o;
;T;["X&.x-item-selected.x-list-item-tpl,
            &.x-item-selected .x-dock-horizontal;@	;o;;" ;	ii;[o;;[o;;@=;	ii;[o;;@=;	iio;;["x-item-selected;@=;	iio;;["x-list-item-tpl;@=;	iio;;["
o;;@=;	ii;[o;;@=;	iio;;["x-item-selected;@=;	iio;;@=;	ii;[o;;["x-dock-horizontal;@=;	ii;[o;;["border-top-color;;;@	;[ ;o; 	;"list_active_color;"list-active-color;@	;	ij;	ij;i ;	ii;i ;i_;i ;iF;i o;;T;[".x-list-round;@;o;;" ;ip;[o;;[o;;@�;ip;[o;;["x-list-round;@�;ip;	[o;;T;[".x-scroll-view;@;o;;" ;iq;[o;;[o;;@�;iq;[o;;["x-scroll-view;@�;iq;	[o;;["background-color;;;@;	[ ;o;; ;!;@;"	#eee;ir;i ;iq;i o;;T;[".x-list-header-swap;@;o;;" ;iu;[o;;[o;;@�;iu;[o;;["x-list-header-swap;@�;iu;	[o;;["padding-right;;;@;	[ ;o;	;"list_round_padding;"list-round-padding;@;iv;iv;i ;iu;i o;;T;["&.x-list-inner .x-scroll-container;@;o;;" ;iy;[o;;[o;;@�;iy;[o;;["x-list-inner;@�;iyo;;@�;iy;[o;;["x-scroll-container;@�;iy;	[o;;["top;;;@;	[ ;o;	;"list_round_padding;"list-round-padding;@;iz;iz;i o;;["	left;;;@;	[ ;o;	;"list_round_padding;"list-round-padding;@;i{;i{;i o;;["bottom;;;@;	[ ;o;	;"list_round_padding;"list-round-padding;@;i|;i|;i o;;["
right;;;@;	[ ;o;	;"list_round_padding;"list-round-padding;@;i};i};i o;;["
width;;;@;	[ ;o;; ;!;@;"auto !important;i~;i o;;["height;;;@;	[ ;o;; ;!;@;"auto !important;i;i ;iy;i o;;T;[".x-list-header;@;o;;" ;i};[o;;[o;;@;i};[o;;["x-list-header;@;i};	[o;;["
color;;;@;	[ ;o;; ;!;@;"	#777;i~;i o;;["font-size;;;@;	[ ;o;; ;!;@;"1em;i;i o;;["font-weight;;;@;	[ ;o;; ;!;@;"	bold;i�;i o;;["padding-left;;;@;	[ ;o;; ;!;@;"	26px;i�;i o;;["line-height;;;@;	[ ;o;; ;!;@;"
1.7em;i�;i o;";"background-image;#{ ;@;	[ ;i�;$[o;%
;"linear-gradient;#{ ;@;i�;$[	o;	; ;!;@;"top;i�o;%
;"	rgba;#{ ;@;i�;$[	o;&;'[ ;@;("238;)@�;i�;i�o;&;'[ ;@;("238;)@�;i�;i�o;&;'[ ;@;("238;)@�;i�;i�o;&;'[ ;@;("1;)@�;i;i�o;.	;/;0;@;[o;%
;"	rgba;#{ ;@;i�;$[	o;&;'[ ;@;("238;)@�;i�;i�o;&;'[ ;@;("238;)@�;i�;i�o;&;'[ ;@;("238;)@�;i�;i�o;&;'[ ;@;("0.9;)@�;f0.90000000000000002 ��;i�o;&;'["%;@;("30%;)[ ;i#;i�;i�o;%
;"	rgba;#{ ;@;i�;$[	o;&;'[ ;@;("238;)@�;i�;i�o;&;'[ ;@;("238;)@�;i�;i�o;&;'[ ;@;("238;)@�;i�;i�o;&;'[ ;@;("0.4;)@�;f0.40000000000000002 ��;i�;i};i o;;T;[".x-list-container;@;o;;" ;i�;[o;;[o;;@|;i�;[o;;["x-list-container;@|;i�;	[o;;["padding;;;@;	[ ;o;.	;/;0;@;[	o;	;"list_round_padding;"list-round-padding;@;i�o;	;"list_round_padding;"list-round-padding;@;i�o;&;'[ ;@;("0;)@�;i ;i�o;	;"list_round_padding;"list-round-padding;@;i�;i�;i�;i o;;T;[".x-list-header;@;o;;" ;i�;[o;;[o;;@�;i�;[o;;["x-list-header;@�;i�;	[o;;["padding-left;;;@;	[ ;o;	;"list_round_padding;"list-round-padding;@;i�;i�;i o;;["background-image;;;@;	[ ;o;; ;!;@;"	none;i�;i ;i�;i ;i�;i o;;T;["-&.x-list-ungrouped,
    &.x-list-grouped;@;o;;" ;i�;[o;;[o;;@�;i�;[o;*;@�;i�o;;["x-list-ungrouped;@�;i�o;;["
o;;@�;i�;[o;*;@�;i�o;;["x-list-grouped;@�;i�;	[o;;T;[">.x-list-item-tpl,
        .x-list-item .x-dock-horizontal;@;o;;" ;i�;[o;;[o;;@�;i�;[o;;["x-list-item-tpl;@�;i�o;;["
o;;@�;i�;[o;;["x-list-item;@�;i�o;;@�;i�;[o;;["x-dock-horizontal;@�;i�;	[o;;["border;;;@;	[ ;o;.	;/;0;@;[o;&;'["px;@;("1px;)[ ;i;i�o;	; ;!;@;"
solid;i�o;%
;"darken;#{ ;@;i�;$[o;	;"list_bg_color;"list-bg-color;@;i�o;&;'["%;@;("10%;)[ ;i;i�;i�;i�;i o;;["border-width;;;@;	[ ;o;; ;!;@;"1px 1px 0 1px;i�;i o;;["background;;;@;	[ ;o;	;"list_bg_color;"list-bg-color;@;i�;i�;i ;i�;i ;i�;i o;;T;["&.x-list-ungrouped;@;o;;" ;i�;[o;;[o;;@;i�;[o;*;@;i�o;;["x-list-ungrouped;@;i�;	[o;;T;[".x-list-item-first;@;o;;" ;i�;[o;;[o;;@;i�;[o;;["x-list-item-first;@;i�;	[u;1[o:Sass::Script::Variable	:@underscored_name"include_border_radius:
@name"include-border-radius:@options{ :
@linei�0[o:Sass::Tree::MixinNode;"border-top-radius:@keywords{ ;@	:@children[ ;	i�:
@args[o; 	;"list_rounded_radius;"list-rounded-radius;@	;	i�;i�;i o;;T;[".x-list-item-last;@;o;;" ;i�;[o;;[o;;@.;i�;[o;;["x-list-item-last;@.;i�;	[u;1[o:Sass::Script::Variable	:@underscored_name"include_border_radius:
@name"include-border-radius:@options{ :
@linei�0[o:Sass::Tree::MixinNode;"border-bottom-radius:@keywords{ ;@	:@children[ ;	i�:
@args[o; 	;"list_rounded_radius;"list-rounded-radius;@	;	i�o;;["border-width;;;@;	[ ;o;; ;!;@;"1px;i�;i o;;["margin-bottom;;;@;	[ ;o;	;"list_round_padding;"list-round-padding;@;i�;i�;i ;i�;i ;i�;i o;;T;["&.x-list-grouped;@;o;;" ;i�;[o;;[o;;@J;i�;[o;*;@J;i�o;;["x-list-grouped;@J;i�;	[o;;T;[".x-list-header-wrap;@;o;;" ;i�;[o;;[o;;@Y;i�;[o;;["x-list-header-wrap;@Y;i�;	[o;;T;[".x-dock-horizontal;@;o;;" ;i�;[o;;[o;;@g;i�;[o;;["x-dock-horizontal;@g;i�;	[u;1[o:Sass::Script::Variable	:@underscored_name"include_border_radius:
@name"include-border-radius:@options{ :
@linei�0[o:Sass::Tree::MixinNode;"border-top-radius:@keywords{ ;@	:@children[ ;	i�:
@args[o; 	;"list_rounded_radius;"list-rounded-radius;@	;	i�;i�;i ;i�;i o;;T;["&.x-list-header-wrap.x-list-header;@;o;;" ;i�;[o;;[o;;@v;i�;[o;;["x-list-header-wrap;@v;i�o;;["x-list-header;@v;i�;	[o;;["border;;;@;	[ ;o;.	;/;0;@;[o;&;'["px;@;("1px;)[ ;i;i�o;	; ;!;@;"
solid;i�o;%
;"darken;#{ ;@;i�;$[o;	;"list_bg_color;"list-bg-color;@;i�o;&;'["%;@;("10%;)[ ;i;i�;i�;i�;i o;;["border-width;;;@;	[ ;o;; ;!;@;"1px 1px 0 1px;i�;i u;1[o:Sass::Script::Variable	:@underscored_name"include_border_radius:
@name"include-border-radius:@options{ :
@linei�0[o:Sass::Tree::MixinNode;"border-top-radius:@keywords{ ;@	:@children[ ;	i�:
@args[o; 	;"list_rounded_radius;"list-rounded-radius;@	;	i�;i�;i o;;T;[".x-list-footer-wrap;@;o;;" ;i�;[o;;[o;;@�;i�;[o;;["x-list-footer-wrap;@�;i�;	[o;;["background;;;@;	[ ;o;; ;!;@;"transparent;i�;i o;;T;["6&.x-list-item-tpl,
            .x-dock-horizontal;@;o;;" ;i�;[o;;[o;;@�;i�;[o;*;@�;i�o;;["x-list-item-tpl;@�;i�o;;["
o;;@�;i�;[o;;["x-dock-horizontal;@�;i�;	[
o;;["border;;;@;	[ ;o;; ;!;@;"	none;i�;i o;;["background;;;@;	[ ;o;; ;!;@;"transparent;i�;i o;;["padding-bottom;;;@;	[ ;o;	;"list_round_padding;"list-round-padding;@;i�;i�;i o;;["margin-bottom;;;@;	[ ;o;	;"list_round_padding;"list-round-padding;@;i�;i�;i o;;T;["3> .x-innerhtml,
                > .x-dock-body;@;o;;" ;i�;[o;;[">o;;@�;i�;[o;;["x-innerhtml;@�;i�o;;["
">o;;@�;i�;[o;;["x-dock-body;@�;i�;	[o;;["border;;;@;	[ ;o;.	;/;0;@;[o;&;'["px;@;("1px;)[ ;i;i�o;	; ;!;@;"
solid;i�o;%
;"darken;#{ ;@;i�;$[o;	;"list_bg_color;"list-bg-color;@;i�o;&;'["%;@;("10%;)[ ;i;i�;i�;i�;i o;;["background;;;@;	[ ;o;	;"list_bg_color;"list-bg-color;@;i�;i�;i u;1[o:Sass::Script::Variable	:@underscored_name"include_border_radius:
@name"include-border-radius:@options{ :
@linei�0[o:Sass::Tree::MixinNode;"border-bottom-radius:@keywords{ ;@	:@children[ ;	i�:
@args[o; 	;"list_rounded_radius;"list-rounded-radius;@	;	i�;i�;i ;i�;i o;;T;["&.x-item-selected;@;o;;" ;i�;[o;;[o;;@%;i�;[o;*;@%;i�o;;["x-item-selected;@%;i�;	[o;;T;["3> .x-innerhtml,
                > .x-dock-body;@;o;;" ;i�;[o;;[">o;;@4;i�;[o;;["x-innerhtml;@4;i�o;;["
">o;;@4;i�;[o;;["x-dock-body;@4;i�;	[o;";"background-gradient;#{ ;@;	[ ;i�;$[o;	;"list_active_color;"list-active-color;@;i�o;	;"list_active_gradient;"list-active-gradient;@;i�o;";"color-by-background;#{ ;@;	[ ;i�;$[o;	;"list_active_color;"list-active-color;@;i�;i�;i ;i�;i ;i�;i ;i�;i o;;T;[".x-indexbar-vertical;@;o;;" ;i�;[o;;[o;;@_;i�;[o;;["x-indexbar-vertical;@_;i�;	[o;;["margin-right;;;@;	[ ;o;; ;!;@;"	20px;i�;i ;i�;i ;ip;i o;;T;["c.x-list-round .x-list-footer-wrap.x-list-item-last.x-list-item-odd.x-list-item.x-list-item-tpl;@;o;;" ;i�;[o;;[o;;@s;i�;[o;;["x-list-round;@s;i�o;;@s;i�;[
o;;["x-list-footer-wrap;@s;i�o;;["x-list-item-last;@s;i�o;;["x-list-item-odd;@s;i�o;;["x-list-item;@s;i�o;;["x-list-item-tpl;@s;i�;	[o;;["background-color;;;@;	[ ;o;; ;!;@;"transparent !important;i�;i ;i�;i o;;T;["E.x-list-round.x-list-grouped .x-list-item-odd.x-list-footer-wrap;@;o;;" ;i�;[o;;[o;;@�;i�;[o;;["x-list-round;@�;i�o;;["x-list-grouped;@�;i�o;;@�;i�;[o;;["x-list-item-odd;@�;i�o;;["x-list-footer-wrap;@�;i�;	[o;;T;["'> .x-innerhtml,
    > .x-dock-body;@;o;;" ;i�;[o;;[">o;;@�;i�;[o;;["x-innerhtml;@�;i�o;;["
">o;;@�;i�;[o;;["x-dock-body;@�;i�;	[o;;["background-color;;;@;	[ ;o;.	;/;0;@;[o;%
;"darken;#{ ;@;i�;$[o;	;"list_bg_color;"list-bg-color;@;i�o;&;'["%;@;("5%;)[ ;i
;i�o;	; ;!;@;"!important;i�;i�;i�;i ;i�;i ;i�;i o;;T;[".x-list .x-list-item-odd;@;o;;" ;i�;[o;;[o;;@�;i�;[o;;["x-list;@�;i�o;;@�;i�;[o;;["x-list-item-odd;@�;i�;	[o;;T;[".&.x-list-item-tpl,
    .x-dock-horizontal;@;o;;" ;i�;[o;;[o;;@�;i�;[o;*;@�;i�o;;["x-list-item-tpl;@�;i�o;;["
o;;@�;i�;[o;;["x-dock-horizontal;@�;i�;	[o;;["background-color;;;@;	[ ;o;.	;/;0;@;[o;%
;"darken;#{ ;@;i�;$[o;	;"list_bg_color;"list-bg-color;@;i�o;&;'["%;@;("5%;)[ ;i
;i�o;	; ;!;@;"!important;i�;i�;i�;i o;;["border-bottom;;;@;	[ ;o;.	;/;0;@;[o;&;'["px;@;("1px;)[ ;i;i�o;	; ;!;@;"
solid;i�o;%
;"darken;#{ ;@;i�;$[o;	;"list_bg_color;"list-bg-color;@;i�o;&;'["%;@;("5%;)[ ;i
;i�;i�;i�;i ;i�;i ;i�;i ;i
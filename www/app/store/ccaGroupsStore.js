Ext.define('app.store.ccaGroupsStore', {
    extend: 'Ext.data.Store',


    requires: [
        'app.model.ccaGroupsModel'
    ],

    config: {
        model: 'app.model.ccaGroupsModel',
        autoLoad: true,

        proxy: {
        type: 'ajax',
        url : 'http://cca.sg/spcca/sp-cca-groups.xml',
        reader: {
            type: 'xml',
		    root  : 'sp_cca_groups',
            record: 'organisation'
				}
		},


        sorters: [{
            property : "name",
            direction: "ASC"
        }],
			grouper: {
         groupFn: function(record) {
             return record.get('name')[0];
         }
     },

    }
});
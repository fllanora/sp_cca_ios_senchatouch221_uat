Ext.define('app.store.ccaFeedsItemStore', {
    extend: 'Ext.data.Store',


    requires: [
        'app.model.ccaFeedsItemModel'
    ],

    config: {
        model: 'app.model.ccaFeedsItemModel',
       autoLoad:false,
	 proxy: {
        type: 'ajax',
        url : 'http://cca.sg/feed/',
        reader: {
            type: 'xml',
		    root  : 'channel',
            record: 'item'
        }
    }
	


    }
});
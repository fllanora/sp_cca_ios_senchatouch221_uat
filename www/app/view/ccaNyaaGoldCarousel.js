

Ext.define('app.view.ccaNyaaGoldCarousel', {
    extend: 'Ext.Carousel',
    xtype: 'ccaNyaaGoldCarousel',
	id: 'ccaNyaaGoldCarousel',
	 defaults: {
        styleHtmlContent: true
    },
/*	style: 'color:#FFFFFF', */
    style: 'background-color:#000000',
           
	
    config: {
			style: {
            color: 'red'
            },
    },
	initialize: function()
	{
		getData();
	}

});



/*****************************************************
 * Process Gold Request
 *****************************************************/
function processGoldRequest(xmlString){

    var nyaaGoldComponentArr = new Array();
    var outputStr = xmlString;
    var childEleLength = parseFromString(outputStr).childNodes[0].childElementCount;
    for (var eleCounter = 0; eleCounter < childEleLength; eleCounter++) {
        var nyaaGoldItemHash = new Object;
        nyaaGoldItemHash.id = parseFromString(outputStr).getElementsByTagName('id')[eleCounter].childNodes[0].nodeValue;
        nyaaGoldItemHash.name = parseFromString(outputStr).getElementsByTagName('name')[eleCounter].childNodes[0].nodeValue;
        nyaaGoldItemHash.desc = parseFromString(outputStr).getElementsByTagName('desc')[eleCounter].childNodes[0].nodeValue;
        nyaaGoldItemHash.linkname = parseFromString(outputStr).getElementsByTagName('linkname')[eleCounter].childNodes[0].nodeValue;
        nyaaGoldItemHash.linkurl = parseFromString(outputStr).getElementsByTagName('linkurl')[eleCounter].childNodes[0].nodeValue;
        
        nyaaGoldComponentArr[eleCounter] = nyaaGoldItemHash;
    }

    for (var btnItemCounter = 0; btnItemCounter < nyaaGoldComponentArr.length; btnItemCounter++) {
    
	console.log("print values: "+nyaaGoldComponentArr[btnItemCounter].name);
		console.log("print desc: "+nyaaGoldComponentArr[btnItemCounter].desc);
	 
        
         if (screenWidth >= 700) {
         Ext.getCmp('ccaNyaaGoldCarousel').add(new Ext.Panel({
         styleHtmlContent: true,
         html:  Ext.XTemplate('<center><div style=color:#ffffff;font-weight:bold;font-size:1.80em>' + nyaaGoldComponentArr[btnItemCounter].name + '</div></center><div style=color:#ffffff;padding:15px;font-size:1.50em>' + nyaaGoldComponentArr[btnItemCounter].desc + '</div><br/>'),
         style: 'background-color: #000000'}));
         }
         else
         {
         Ext.getCmp('ccaNyaaGoldCarousel').add(new Ext.Panel({
         styleHtmlContent: true,
         html:  Ext.XTemplate('<center><div style=color:#ffffff;font-weight:bold>' + nyaaGoldComponentArr[btnItemCounter].name + '</div></center><div style=color:#ffffff;padding:15px>' + nyaaGoldComponentArr[btnItemCounter].desc + '</div><br/>'),
         style: 'background-color: #000000'}));
         }
         

    }
	
    
} // end of processGoldRequest


/*****************************************************
 * OParse From String
 *****************************************************/
function parseFromString(xml){
    if (window.DOMParser) {
        return new DOMParser().parseFromString(xml, 'text/xml');
    }
    if (window.ActiveXObject) {
        var doc = new ActiveXObject('Microsoft.XMLDOM');
        doc.async = 'false';
        doc.loadXML(xml);
        return doc;
    }
}


function getData()
	{

	 Ext.Ajax.request({
                  //  url: 'https://mobileweb.sp.edu.sg/ccaws/student/studentnyaa/ccastudentnyaa.jsp',
                      url: 'https://mobileweb-tst.testsf.testsp.edu.sg/ccaws/student/studentnyaa/ccastudentnyaa.jsp',
                      
                      //url: 'http://fritzllanora.com/sp/ccastudentnyaa.xml',
                    success: function(response, opts){
                        var outputStr = response.responseText;
						
						processGoldRequest(outputStr);
						
						
                    },
                    failure: function(response, opts){
                        var myCacheString = "<?xml version=\"1.0\"?><components><component><id>1</id><name>Service</name><desc>Volunteer for at least 60 hours spread over at least 12 months</desc><linkname>Service… more...</linkname><linkurl>http://www.sp.edu.sg/</linkurl></component><component><id>2</id><name>Adventurous Journey</name><desc>4D3N outdoor camp with at least 32 hours of outdoor activities, Example LEAP@SP!</desc><linkname>Adventure… more…</linkname><linkurl>http://www.sp.edu.sg/</linkurl></component><component><id>3</id><name>Skills Development</name><desc>Complete and improve on a skill for at least 18 months on a regular consistent basis</desc><linkname>Skills Development… more...</linkname><linkurl>http://www.sp.edu.sg/</linkurl></component><component><id>4</id><name>Physical Recreation</name><desc>Take up  and indulge in a sport for at least 40 hours spread over 3 months, Example Sports Elective</desc><linkname>Leadership… more...</linkname><linkurl>http://www.sp.edu.sg/</linkurl></component><component><id>5</id><name>Residential Project</name><desc>Plan, undertake, follow up and sustain a meaningful project that can benefit the less fortunate community at large, Example OCIPs</desc><linkname>Community… more...</linkname><linkurl>http://www.sp.edu.sg/</linkurl></component></components>";
                        processGoldRequest(myCacheString);
                       
                    }                });
				
	


	}
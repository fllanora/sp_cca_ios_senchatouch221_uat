Ext.define('app.view.ccaScoresCardView', {
    extend: 'Ext.Panel',
    xtype: 'ccaScoresCardView',
           requires: [
                      'app.view.ccaScoresView',
                        'app.view.ccaComponentsView',
                      ],
    
    config: {
        itemId: 'ccaScoresCardView',
           id: 'ccaScoresCardView',
      
        title: 'CCA Scores',
        iconCls: 'iconScore',
       layout: 'card',
           items:[{xtype: 'ccaScoresView', id: 'ccaScoresView'}, {xtype: 'ccaComponentsView', id: 'ccaComponentsView'}]

    }
});

